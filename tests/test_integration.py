"""Integration tests"""

import pytest
import os
import filecmp
import h5py
import subprocess

import pyframe
import pyframe.readers
from pyframe.readers import PDBError

def strip_version_from_file(filename):
    subprocess.check_output(["sed", "-i", "s/ (version.*)//", filename])


def test_pdb_error():
    test = 'pdb_error'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    with pytest.warns(UserWarning):
        pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test))


def test_permanganate():
    test = 'permanganate'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    core = system.get_fragments_by_name('LIG')
    system.set_core_region(core)
    solvent = system.get_fragments_by_name(names=['HOH'])
    system.add_region(name='solvent',
                      fragments=solvent,
                      use_standard_potentials=True,
                      standard_potential_model='TIP3P')
    project.create_embedding_potential(system)
    project.write_core(system)
    assert os.path.isfile('{0}/{1}/{1}.mol'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.mol'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.mol'.format(tests_dir, test), '{0}/{1}/{1}.mol.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.mol'.format(tests_dir, test))
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_pna_in_ccl4():
    test = 'PNA_in_CCl4'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    core = system.get_fragments_by_name(names=['PNA'])
    system.set_core_region(core)
    solvent = system.get_fragments_by_name(names=['TET'])
    system.add_region(name='solvent', fragments=solvent, use_standard_potentials=True, standard_potential_model='SEP')
    project.create_embedding_potential(system)
    project.write_core(system)
    assert os.path.isfile('{0}/{1}/{1}.mol'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.mol'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.mol'.format(tests_dir, test), '{0}/{1}/{1}.mol.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.mol'.format(tests_dir, test))
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_4np_in_water():
    test = '4NP_in_water'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    core = system.get_fragments_by_name(names=['4NP'])
    system.set_core_region(core)
    ions = system.get_fragments_by_number(numbers=[2, *range(3, 8), 8, 9])
    ions += system.get_fragments_by_number(10)
    system.add_region(name='ions', fragments=ions, use_standard_potentials=True, standard_potential_model='SEP')
    tip3p = system.get_fragments_by_number(numbers=[*range(11, 16)])
    system.add_region(name='tip3p', fragments=tip3p, use_standard_potentials=True, standard_potential_model='TIP3P')
    solvent = system.get_fragments_by_name(names=['WAT'])
    system.add_region(name='solvent', fragments=solvent, use_standard_potentials=True, standard_potential_model='SEP')
    project.create_embedding_potential(system)
    project.write_core(system)
    assert os.path.isfile('{0}/{1}/{1}.mol'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.mol'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.mol'.format(tests_dir, test), '{0}/{1}/{1}.mol.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.mol'.format(tests_dir, test))
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_insulin():
    test = 'insulin'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    protein = system.get_fragments_by_chain_id(chain_ids=['A', 'B'])
    system.add_region(name='protein',
                      fragments=protein,
                      use_mfcc=True,
                      use_multipoles=True,
                      multipole_order=2,
                      xcfun='PBE0',
                      basis='loprop-cc-pVDZ',
                      use_polarizabilities=True,
                      isotropic_polarizabilities=True)
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    potential = pyframe.readers.read_pelib_potential('{0}/{1}/{1}.pot'.format(tests_dir, test))
    reference_potential = pyframe.readers.read_pelib_potential('{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    for site, ref_site in zip(potential.values(), reference_potential.values()):
        assert site.element == ref_site.element
        for comp, ref_comp in zip(site.coordinate, ref_site.coordinate):
            assert pytest.approx(comp) == ref_comp
        for comp, ref_comp in zip(site.M0, ref_site.M0):
            assert pytest.approx(comp) == ref_comp
        for comp, ref_comp in zip(site.M1, ref_site.M1):
            assert pytest.approx(comp) == ref_comp
        for comp, ref_comp in zip(site.M2, ref_site.M2):
            assert pytest.approx(comp) == ref_comp
        for comp, ref_comp in zip(site.P11, ref_site.P11):
            assert pytest.approx(comp) == ref_comp
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_insulin_cp3_fragment():
    test = 'insulin_cp3_fragment'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    protein = system.get_fragments_by_chain_id(chain_ids=['A', 'B'])
    system.add_region(name='protein',
                      fragments=protein,
                      use_standard_potentials=True,
                      standard_potential_model='CP3',
                      standard_potential_exclusion_type='fragment')
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_insulin_cp3_mfcc():
    test = 'insulin_cp3_mfcc'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    protein = system.get_fragments_by_chain_id(chain_ids=['A', 'B'])
    system.add_region(name='protein',
                      fragments=protein,
                      use_standard_potentials=True,
                      standard_potential_model='CP3',
                      standard_potential_exclusion_type='mfcc')
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_insulin_ff94():
    test = 'insulin_ff94'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    protein = system.get_fragments_by_chain_id(chain_ids=['A', 'B'])
    system.add_region(name='protein',
                      fragments=protein,
                      use_standard_potentials=True,
                      standard_potential_model='ff94')
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_insulin_ff03():
    test = 'insulin_ff03'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    protein = system.get_fragments_by_chain_id(chain_ids=['A', 'B'])
    system.add_region(name='protein',
                      fragments=protein,
                      use_standard_potentials=True,
                      standard_potential_model='ff03')
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_4val():
    test = '4VAL'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    mfcc = system.get_fragments_by_chain_id('A')
    system.add_region(name='mfcc',
                      fragments=mfcc,
                      use_mfcc=True,
                      use_multipoles=True,
                      multipole_order=2,
                      use_polarizabilities=True)
    cp3 = system.get_fragments_by_chain_id(chain_ids=['B'])
    system.add_region(name='cp3', fragments=cp3, use_standard_potentials=True, standard_potential_model='CP3')
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_4val_mfcc_1():
    test = '4VAL_MFCC-1'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    mfcc = system.get_fragments_by_chain_id('A')
    system.add_region(name='mfcc',
                      fragments=mfcc,
                      use_mfcc=True,
                      mfcc_order=1,
                      use_multipoles=True,
                      multipole_order=2,
                      use_polarizabilities=True,
                      method='HF',
                      basis='STO-3G')
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))


def test_popc_alep():
    test = 'popc_alep'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    system.split_fragment_by_name(
        name='POPC',
        new_names=['POCH', 'POCO', 'POCP'],
        fragment_definitions=[['N', 'C13', 'H13A', 'H13B', 'H13C', 'C14', 'H14A', 'H14B', 'H14C', 'C15', 'H15A', 'H15B',
                               'H15C', 'C12', 'H12A', 'H12B', 'C11', 'H11A', 'H11B', 'P', 'O11', 'O12', 'O13', 'O14',
                               'C1', 'HA', 'HB', 'C2', 'HS', 'O21', 'C21', 'O22', 'C3', 'HX', 'HY', 'O31', 'C31', 'O32'
                              ],
                              [
                                  'C22', 'H2R', 'H2S', 'C23', 'H3R', 'H3S', 'C24', 'H4R', 'H4S', 'C25', 'H5R', 'H5S',
                                  'C26', 'H6R', 'H6S', 'C27', 'H7R', 'H7S', 'C28', 'H8R', 'H8S', 'C29', 'H91', 'C210',
                                  'H101', 'C211', 'H11R', 'H11S', 'C212', 'H12R', 'H12S', 'C213', 'H13R', 'H13S',
                                  'C214', 'H14R', 'H14S', 'C215', 'H15R', 'H15S', 'C216', 'H16R', 'H16S', 'C217',
                                  'H17R', 'H17S', 'C218', 'H18R', 'H18S', 'H18T'
                              ],
                              [
                                  'C32', 'H2X', 'H2Y', 'C33', 'H3X', 'H3Y', 'C34', 'H4X', 'H4Y', 'C35', 'H5X', 'H5Y',
                                  'C36', 'H6X', 'H6Y', 'C37', 'H7X', 'H7Y', 'C38', 'H8X', 'H8Y', 'C39', 'H9X', 'H9Y',
                                  'C310', 'H10X', 'H10Y', 'C311', 'H11X', 'H11Y', 'C312', 'H12X', 'H12Y', 'C313',
                                  'H13X', 'H13Y', 'C314', 'H14X', 'H14Y', 'C315', 'H15X', 'H15Y', 'C316', 'H16X',
                                  'H16Y', 'H16Z'
                              ]])
    lipid = system.get_fragments_by_name(names=['POCH', 'POCO', 'POCP'])
    system.add_region(name='lipid',
                      fragments=lipid,
                      use_standard_potentials=True,
                      standard_potential_model='ALEP',
                      standard_potential_exclusion_type='mfcc',
                      use_mfcc=True,
                      mfcc_order=3)
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile(f'{tests_dir}/{test}/{test}.pot')
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp(f'{tests_dir}/{test}/{test}.pot', f'{tests_dir}/{test}/{test}.pot.ref')
    os.remove(f'{tests_dir}/{test}/{test}.pot')


def test_popc_lipid14():
    test = 'popc_lipid14'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    system.split_fragment_by_name(
        name='POPC',
        new_names=['POCH', 'POCO', 'POCP'],
        fragment_definitions=[['N', 'C13', 'H13A', 'H13B', 'H13C', 'C14', 'H14A', 'H14B', 'H14C', 'C15', 'H15A', 'H15B',
                               'H15C', 'C12', 'H12A', 'H12B', 'C11', 'H11A', 'H11B', 'P', 'O11', 'O12', 'O13', 'O14',
                               'C1', 'HA', 'HB', 'C2', 'HS', 'O21', 'C21', 'O22', 'C3', 'HX', 'HY', 'O31', 'C31', 'O32'
                              ],
                              [
                                  'C22', 'H2R', 'H2S', 'C23', 'H3R', 'H3S', 'C24', 'H4R', 'H4S', 'C25', 'H5R', 'H5S',
                                  'C26', 'H6R', 'H6S', 'C27', 'H7R', 'H7S', 'C28', 'H8R', 'H8S', 'C29', 'H91', 'C210',
                                  'H101', 'C211', 'H11R', 'H11S', 'C212', 'H12R', 'H12S', 'C213', 'H13R', 'H13S',
                                  'C214', 'H14R', 'H14S', 'C215', 'H15R', 'H15S', 'C216', 'H16R', 'H16S', 'C217',
                                  'H17R', 'H17S', 'C218', 'H18R', 'H18S', 'H18T'
                              ],
                              [
                                  'C32', 'H2X', 'H2Y', 'C33', 'H3X', 'H3Y', 'C34', 'H4X', 'H4Y', 'C35', 'H5X', 'H5Y',
                                  'C36', 'H6X', 'H6Y', 'C37', 'H7X', 'H7Y', 'C38', 'H8X', 'H8Y', 'C39', 'H9X', 'H9Y',
                                  'C310', 'H10X', 'H10Y', 'C311', 'H11X', 'H11Y', 'C312', 'H12X', 'H12Y', 'C313',
                                  'H13X', 'H13Y', 'C314', 'H14X', 'H14Y', 'C315', 'H15X', 'H15Y', 'C316', 'H16X',
                                  'H16Y', 'H16Z'
                              ]])
    lipid = system.get_fragments_by_name(names=['POCH', 'POCO', 'POCP'])
    system.add_region(name='lipid',
                      fragments=lipid,
                      use_standard_potentials=True,
                      standard_potential_model='Lipid14')
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile(f'{tests_dir}/{test}/{test}.pot')
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp(f'{tests_dir}/{test}/{test}.pot', f'{tests_dir}/{test}/{test}.pot.ref')
    os.remove(f'{tests_dir}/{test}/{test}.pot')


def test_gfp():
    test = 'GFP'
    tests_dir = f'{os.path.dirname(__file__)}'
    project = pyframe.Project(work_dir=f'{tests_dir}')
    system = pyframe.MolecularSystem(input_file=f'{tests_dir}/{test}/{test}.pdb')
    core = system.get_fragments_by_identifier(identifiers=[
        '63_CRO', '40_LEU', '141_TYR', '161_PHE', '60_THR', '59_VAL', '62_LEU', '64_VAL', '144_HID', '218_GLH',
        '92_ARG', '199_THR', '201_SER', '90_GLN', '65_GLN', '467_WAT', '303_WAT', '263_WAT', '364_WAT', '340_WAT',
        '349_WAT', '518_WAT'
    ])
    system.set_core_region(core)
    project.write_core(system)
    assert os.path.isfile('{0}/{1}/{1}.mol'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.mol'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.mol'.format(tests_dir, test), '{0}/{1}/{1}.mol.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.mol'.format(tests_dir, test))


def test_pde_simple_water():
    test = 'pde_simple_water'
    tests_dir = f'{os.path.dirname(__file__)}'
    project = pyframe.Project(work_dir=f'{tests_dir}', mpi_procs_per_job=1, jobs_per_node=1)
    system = pyframe.MolecularSystem(input_file=f'{tests_dir}/{test}/{test}.pdb')
    environment = system.get_fragments_by_name(['WAT'])
    core = system.get_fragments_by_name(['COR'])
    system.set_core_region(core, basis='STO-3G')
    system.add_region(name='environment',
                      fragments=environment,
                      use_mfcc=False,
                      use_multipoles=False,
                      use_polarizabilities=True,
                      use_fragment_densities=True,
                      use_exchange_repulsion=True,
                      method='HF',
                      basis='loprop-6-31+G*',
                      exchange_repulsion_factor=1.0)
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile(f'{tests_dir}/{test}/{test}.pot')
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp(f'{tests_dir}/{test}/{test}.pot', f'{tests_dir}/{test}/{test}.pot.ref')
    os.remove(f'{tests_dir}/{test}/{test}.pot')
    assert os.path.isfile(f'{tests_dir}/{test}/{test}.h5')
    h5 = h5py.File(f'{tests_dir}/{test}/{test}.h5', 'r')
    h5ref = h5py.File(f'{tests_dir}/{test}/{test}.h5.ref', 'r')
    assert len(h5.keys()) == len(h5ref.keys())
    for key, ref_key in zip(h5.keys(), h5ref.keys()):
        assert key == ref_key
        assert h5[key].dtype == h5ref[ref_key].dtype
        assert h5[key].shape == h5ref[ref_key].shape
        assert h5[key].size == h5ref[ref_key].size
        if h5[key].size > 1:
            for value, ref_value in zip(h5[key], h5ref[ref_key]):
                assert pytest.approx(value) == ref_value
        else:
            assert pytest.approx(h5[key][()]) == h5ref[ref_key][()]
    h5.close()
    h5ref.close()
    os.remove(f'{tests_dir}/{test}/{test}.h5')
    project.write_core(system)
    assert os.path.isfile(f'{tests_dir}/{test}/{test}.mol')
    strip_version_from_file('{0}/{1}/{1}.mol'.format(tests_dir, test))
    assert filecmp.cmp(f'{tests_dir}/{test}/{test}.mol', f'{tests_dir}/{test}/{test}.mol.ref')
    os.remove(f'{tests_dir}/{test}/{test}.mol')


def test_pde_two_water_helium():
    test = 'pde_two_water_helium'
    tests_dir = f'{os.path.dirname(__file__)}'
    project = pyframe.Project(work_dir=f'{tests_dir}', mpi_procs_per_job=1, jobs_per_node=1)
    system = pyframe.MolecularSystem(input_file=f'{tests_dir}/{test}/{test}.pdb')
    environment = system.get_fragments_by_name(['WAT'])
    core = system.get_fragments_by_name(['HEL'])
    system.set_core_region(core, basis='6-31+G*')
    system.add_region(name='environment',
                      fragments=environment,
                      use_mfcc=False,
                      use_multipoles=False,
                      use_polarizabilities=True,
                      use_fragment_densities=True,
                      use_exchange_repulsion=True,
                      basis='loprop-6-31+G*',
                      exchange_repulsion_factor=1.0)
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert os.path.isfile(f'{tests_dir}/{test}/{test}.h5')
    h5 = h5py.File(f'{tests_dir}/{test}/{test}.h5', 'r')
    h5ref = h5py.File(f'{tests_dir}/{test}/{test}.h5.ref', 'r')
    assert len(h5.keys()) == len(h5ref.keys())
    for key, ref_key in zip(h5.keys(), h5ref.keys()):
        assert key == ref_key
        assert h5[key].dtype == h5ref[ref_key].dtype
        assert h5[key].shape == h5ref[ref_key].shape
        assert h5[key].size == h5ref[ref_key].size
        if h5[key].size > 1:
            for value, ref_value in zip(h5[key], h5ref[ref_key]):
                assert pytest.approx(value) == ref_value
        else:
            assert pytest.approx(h5[key][()]) == h5ref[ref_key][()]
    os.remove(f'{tests_dir}/{test}/{test}.h5')
    project.write_core(system)
    assert os.path.isfile('{0}/{1}/{1}.mol'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.mol'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.mol'.format(tests_dir, test), '{0}/{1}/{1}.mol.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.mol'.format(tests_dir, test))


def test_pde_GG_acetone():
    test = 'pde_GG_acetone'
    tests_dir = f'{os.path.dirname(__file__)}'
    project = pyframe.Project(work_dir=f'{tests_dir}', mpi_procs_per_job=1, jobs_per_node=1)
    system = pyframe.MolecularSystem(input_file=f'{tests_dir}/{test}/{test}.pdb')
    peptide = system.get_fragments_by_chain_id(['A'])
    ligand = system.get_fragments_by_chain_id(['B'])
    system.set_core_region(ligand)
    system.add_region(name='peptide',
                      fragments=peptide,
                      use_mfcc=True,
                      use_multipoles=False,
                      use_polarizabilities=True,
                      use_fragment_densities=True,
                      use_exchange_repulsion=True,
                      basis='6-31G')
    project.create_embedding_potential(system)
    project.write_potential(system)
    assert os.path.isfile('{0}/{1}/{1}.pot'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.pot'.format(tests_dir, test), '{0}/{1}/{1}.pot.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.pot'.format(tests_dir, test))
    assert os.path.isfile(f'{tests_dir}/{test}/{test}.h5')
    h5 = h5py.File(f'{tests_dir}/{test}/{test}.h5', 'r')
    h5ref = h5py.File(f'{tests_dir}/{test}/{test}.h5.ref', 'r')
    assert len(h5.keys()) == len(h5ref.keys())
    for key, ref_key in zip(h5.keys(), h5ref.keys()):
        assert key == ref_key
        assert h5[key].dtype == h5ref[ref_key].dtype
        assert h5[key].shape == h5ref[ref_key].shape
        assert h5[key].size == h5ref[ref_key].size
        if h5[key].size > 1:
            for value, ref_value in zip(h5[key], h5ref[ref_key]):
                assert pytest.approx(value) == ref_value
        else:
            assert pytest.approx(h5[key][()]) == h5ref[ref_key][()]
    os.remove(f'{tests_dir}/{test}/{test}.h5')
    project.write_core(system)
    assert os.path.isfile('{0}/{1}/{1}.mol'.format(tests_dir, test))
    strip_version_from_file('{0}/{1}/{1}.mol'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.mol'.format(tests_dir, test), '{0}/{1}/{1}.mol.ref'.format(tests_dir, test))
    os.remove('{0}/{1}/{1}.mol'.format(tests_dir, test))


def test_terminal_autodetect():
    test = 'terminal_autodetect'
    tests_dir = f'{os.path.dirname(__file__)}'
    project = pyframe.Project(work_dir=f'{tests_dir}/{test}')
    for aa in [
            'ALA', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY', 'HIS', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO',
            'SER', 'THR', 'TRP', 'TYR', 'VAL'
    ]:
        for prefix in ['', 'N', 'C']:
            system = pyframe.MolecularSystem(input_file=f'{tests_dir}/{test}/{prefix}{aa}.pdb')
            system.add_region(name='protein',
                              fragments=system.fragments,
                              use_standard_potentials=True,
                              standard_potential_model='cp3')
            project.create_embedding_potential(system)
            project.write_potential(system)
            assert os.path.isfile(f'{tests_dir}/{test}/{prefix}{aa}/{prefix}{aa}.pot')
            strip_version_from_file(f'{tests_dir}/{test}/{prefix}{aa}/{prefix}{aa}.pot')
            assert filecmp.cmp(f'{tests_dir}/{test}/{prefix}{aa}/{prefix}{aa}.pot',
                               f'{tests_dir}/{test}/{prefix}{aa}/{prefix}{aa}.pot.ref')
            os.remove(f'{tests_dir}/{test}/{prefix}{aa}/{prefix}{aa}.pot')


def test_pdbreader_element_guess():
    test = 'pdbreader_element_guess'
    tests_dir = f'{os.path.dirname(__file__)}'
    project = pyframe.Project(work_dir=f'{tests_dir}/{test}')
    with pytest.warns(UserWarning):
        system = pyframe.MolecularSystem(input_file=f'{tests_dir}/{test}/GLY_noelement.pdb')
    ref_elements = ['N', 'H', 'C', 'H', 'H', 'C', 'O']
    for atom, ref_element in zip(system.fragments['1_GLY'].atoms, ref_elements):
        assert atom.element == ref_element


def test_reset():
    test = 'permanganate'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    num_fragments = len(system.fragments)
    core = system.get_fragments_by_name('LIG')
    assert len(system.fragments) == num_fragments - 1
    assert system.core_region is None
    system.set_core_region(core)
    assert system.core_region is not None
    solvent = system.get_fragments_by_name(names=['HOH'])
    assert len(system.fragments) == 0
    assert len(system.regions) == 0
    system.add_region(name='solvent',
                      fragments=solvent,
                      use_standard_potentials=True,
                      standard_potential_model='TIP3P')
    assert len(system.regions) == 1
    system.reset()
    assert len(system.fragments) == num_fragments
    assert system.core_region is None
    assert len(system.regions) == 0


def test_split_fragment_by_identifier():
    test = '4VAL'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    system.split_fragment_by_identifier(identifier='1_A_VAL',
                                        new_names=['VALB', 'VALS'],
                                        fragment_definitions=[['N', 'H', 'C', 'O', 'CA', 'HA'], ['.*']])
    assert '1_A_VALB' in system.fragments
    assert '1_A_VALS' in system.fragments
    assert len(system.fragments['1_A_VALB'].atoms) == 6
    assert len(system.fragments['1_A_VALS'].atoms) == 10


def test_split_fragment_by_name():
    test = '4VAL'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    system.split_fragment_by_name(name='VAL',
                                  new_names=['VALB', 'VALS'],
                                  fragment_definitions=[['N', 'H', 'C', 'O', 'CA', 'HA'], ['.*']])
    assert '1_A_VALB' in system.fragments
    assert '3_B_VALS' in system.fragments
    assert len(system.fragments['2_A_VALB'].atoms) == 6
    assert len(system.fragments['4_B_VALS'].atoms) == 10

def test_basis_dict():
    test = '4VAL'
    tests_dir = '{0}'.format(os.path.dirname(__file__))
    project = pyframe.Project(work_dir='{0}'.format(tests_dir))
    system = pyframe.MolecularSystem(input_file='{0}/{1}/{1}.pdb'.format(tests_dir, test), bond_threshold=1.15)
    core = system.get_remaining_fragments()
    system.set_core_region(core, basis={'H': 'STO-3G', 'C':'aug-cc-pVDZ', 'N': '6-31G*', 'O': 'def2_svp'})
    project.write_core(system)
    strip_version_from_file('{0}/{1}/{1}.mol'.format(tests_dir, test))
    assert filecmp.cmp('{0}/{1}/{1}.mol'.format(tests_dir, test), '{0}/{1}/{1}.mol.ref'.format(tests_dir, test))
    with pytest.raises(ValueError):
        system.set_core_region(core, basis={'H': 'STO-3G', 'C':'aug-cc-pVDZ', 'N': '6-31G*'})
